Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ncmpc
Upstream-Contact: Max Kellermann <max.kellermann@gmail.com>
Source: https://www.musicpd.org/

Files: *
Copyright: 2003-2023, The Music Player Daemon Project
License: GPL-2+

Files: debian/*
Copyright: 2018-2025, Geoffroy Youri Berret <kaliko@debian.org>
  2023, Diederik de Haas <didi.debian@cknow.org>
  2016, Alec Leamas <leamas@nowhere.net>
  2006-2017, Sebastian Harl <tokkee@debian.org>
  2005, 2006, René van Bevern <rvb@progn.org>
License: GPL-2+

Files: lyrics/*
Copyright: The Music Player Daemon Project
License: GPL-2+

Files: src/*
Copyright: The Music Player Daemon Project
License: GPL-2+

Files: src/client/*
Copyright: The Music Player Daemon Project
License: BSD-2-clause

Files: src/client/mpdclient.cxx
  src/client/mpdclient.hxx
Copyright: The Music Player Daemon Project
License: GPL-2+

Files: src/co/*
Copyright: CM4all GmbH
License: BSD-2-clause

Files: src/event/*
Copyright: CM4all GmbH
License: BSD-2-clause

Files: src/event/Backend.hxx
  src/event/PollBackend.cxx
  src/event/PollBackend.hxx
  src/event/PollResultGeneric.hxx
  src/event/SignalMonitor.cxx
  src/event/SignalMonitor.hxx
  src/event/WakeFD.hxx
  src/event/WinSelectBackend.cxx
  src/event/WinSelectBackend.hxx
Copyright: The Music Player Daemon Project
License: GPL-2+

Files: src/event/BackendEvents.hxx
  src/event/PollEvents.hxx
  src/event/WinSelectEvents.hxx
Copyright: 2003-2021, The Music Player Daemon Project
License: BSD-2-clause

Files: src/event/Chrono.hxx
  src/event/DeferEvent.cxx
  src/event/DeferEvent.hxx
  src/event/EpollBackend.hxx
  src/event/IdleEvent.hxx
  src/event/SocketEvent.cxx
  src/event/SocketEvent.hxx
Copyright: Max Kellermann <max.kellermann@gmail.com>
License: BSD-2-clause

Files: src/io/*
Copyright: 2009-2022, Max Kellermann <max.kellermann@gmail.com>
License: BSD-2-clause

Files: src/io/Path.hxx
Copyright: The Music Player Daemon Project
License: GPL-2+

Files: src/io/uring/*
Copyright: The Music Player Daemon Project
License: GPL-2+

Files: src/lib/*
Copyright: The Music Player Daemon Project
License: BSD-2-clause

Files: src/net/*
Copyright: 2009-2022, Max Kellermann <max.kellermann@gmail.com>
License: BSD-2-clause

Files: src/net/AsyncResolveConnect.cxx
  src/net/AsyncResolveConnect.hxx
  src/net/Features.hxx
Copyright: The Music Player Daemon Project
License: BSD-2-clause

Files: src/net/HostParser.cxx
  src/net/HostParser.hxx
  src/net/MsgHdr.hxx
  src/net/Resolver.cxx
  src/net/Resolver.hxx
Copyright: CM4all GmbH
License: BSD-2-clause

Files: src/system/*
Copyright: 2009-2022, Max Kellermann <max.kellermann@gmail.com>
License: BSD-2-clause

Files: src/system/EventPipe.cxx
  src/system/EventPipe.hxx
Copyright: The Music Player Daemon Project
License: GPL-2+

Files: src/time/*
Copyright: CM4all GmbH
License: BSD-2-clause

Files: src/util/*
Copyright: 2009-2022, Max Kellermann <max.kellermann@gmail.com>
License: BSD-2-clause

Files: src/util/FNVHash.hxx
  src/util/IntrusiveTreeSet.hxx
  src/util/MemberPointer.hxx
  src/util/OptionalCounter.hxx
  src/util/PrintException.cxx
  src/util/PrintException.hxx
  src/util/RedBlackTree.hxx
  src/util/ReturnValue.hxx
Copyright: CM4all GmbH
License: BSD-2-clause

Files: src/util/StringUTF8.cxx
  src/util/StringUTF8.hxx
  src/util/UriUtil.cxx
  src/util/UriUtil.hxx
Copyright: The Music Player Daemon Project
License: GPL-2+

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 - Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 - Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.
